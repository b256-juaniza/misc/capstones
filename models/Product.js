const mongoose = require("mongoose");

const date = new Date();

const dateTime = (date) => {
		 let hours = date.getHours();
		 let minutes = date.getMinutes();
		 let ap = hours >= 12 ? 'pm' : 'am';
		 hours = hours % 12;
		 hours = hours ? hours : 12;
		 minutes = minutes.toString().padStart(2, '0');
		 let mergeTime = hours + ':' + minutes + ' ' + ap;
	return mergeTime;
}

console.log(dateTime(new Date(2022, 1, 1)));

const prodSchema = new mongoose.Schema({
	image: {
		type: String,
		default: "https://edit.org/images/cat/book-covers-big-2019101610.jpg"
	},
	name: {
		type: String
	},
	description: {
		type: String
	},
	genre: {
		type: String,
	},
	price: {
		type: Number
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: String,
		default: dateTime(date)
	},
	userOrdered: [
	{
		userId: {
			type: String
		},
		orderId: {
			type: String
		},
		name : {
			type: String
		}
	}]
})

module.exports = mongoose.model("Product", prodSchema);